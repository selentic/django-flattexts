from setuptools import setup, find_packages


setup(
    name="django-flattexts",
    version="1.3.1",
    url='https://gitlab.com/selentic/django-flattexts/',
    license='BSD',
    description='Django flattexts',
    long_description=open('README.rst', 'r').read(),
    author='Ingo Berben',
    author_email='ingoberben@selentic.net',
    packages=find_packages('.'),
    install_requires=[
        'django>2,<5.1',
        'django-parler>=2.3,<3',
        'django-summernote>=0.6,<0.9'
    ],
    # package_dir={
    #    '': [
    #        'templates/*',
    #        'templates/*/*',
    #    ],
    # },
    classifiers=[
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Operating System :: OS Independent',
        'Environment :: Web Environment',
        'Framework :: Django',
    ],
)
